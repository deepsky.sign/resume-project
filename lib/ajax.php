<?
ini_set('error_reporting',-1);
ini_set('display_errors',1);

include __DIR__.'/PHPMailer.php';
include __DIR__.'/Exception.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

foreach ($_POST as $key => $value) {
	$data[$key]=stripslashes(trim($value));
}
// email
if (isset($data["email"])&&(filter_var($data["email"], FILTER_VALIDATE_EMAIL)))
  $email = $data["email"];
else  $errors[] = "email";

// name
$name = '/^[a-zA-Zа-яА-Я]+( [a-zA-Zа-яА-Я]+)*$/uism';
if (isset($data["name"])&&(preg_match($name,$data["name"])))
  $name = stripslashes($data["name"]);
else  $errors[] = "name";

// subject
if (isset($data["subject"])&&(!empty($data["subject"])))
  $subject = $data["subject"];
else $errors[] = "subject";

// Question
if (isset($data["question"])&&(!empty($data["question"])))
  $question = stripslashes($data["question"]);
else $errors[] = "question";

$phone_reg = '/^\+[0-9]{1}\s\([0-9]{3}\)\s[0-9]{3}\-[0-9]{4}$/uism';
if (isset($_POST['phone'])&&(preg_match($phone_reg,$_POST['phone'])))
  $phone = stripslashes($_POST['phone']);
else
  $phone = '';

if (!empty($errors)){
	echo json_encode($errors);
	die();
}

  $mail = new PHPMailer(TRUE);
	$mail->CharSet = 'UTF-8';
	$mail->Subject = " Questions from DreamCreditMaker: ".$subject;
	$mail->addAddress('resume@ooo-modern.ru');
	$mail->setFrom('michael_bay@explosions.com','Difinitely not Michael Bay');
	$text = "";
	$text .= "Email: ".$email."\r\n";
	$text .= "Name: ".$name."\r\n";
	$text .= "Question: ".$question."\r\n";
	$text .= "Phone: ".$phone."\r\n";
	$mail->Body =$text;
	if ($mail->send()) echo json_encode("ok");