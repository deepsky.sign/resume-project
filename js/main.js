jQuery(function($){
   $("input[name='phone']").mask("+1 (999) 999-9999");
});
$(document).ready(function(){
	var errors = [];
	forma = $('.writeus-form');
	forma.submit(function(e){
		e.preventDefault();
		var data = forma.serialize();
		$.ajax({
			url: '/lib/ajax.php',
			type: 'post',
			dataType:  'json',
			data: data,
			success: function(data) {
				forma.find('.form-error').removeClass('form-error');
				if(data instanceof Array)
					$.each(data,function(key,value){
						switch(value){
							case 'question':
							case 'phone':
							case 'name':
							case 'email':
								forma.find('*[name="'+value+'"]').addClass('form-error');
								break;
							case 'subject':
								forma.find('*[name="'+value+'"]').siblings('button').addClass('form-error');
								break;
						}
					});
				if(data=='ok'){
					modal_work();
					setTimeout(function(){modal_work()},3000);
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r4343\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});
	function modal_work(){
		$('#mySimpleModal').modal('toggle');
	}
});